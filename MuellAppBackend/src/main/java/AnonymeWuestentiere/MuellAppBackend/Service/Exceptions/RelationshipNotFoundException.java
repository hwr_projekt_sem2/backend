package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;


import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Relationship;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "No Relationship between these two users found")
public class RelationshipNotFoundException extends RuntimeException {

    public RelationshipNotFoundException(int userIdOne, int userIdTwo) {
        super("Relationship between User with id: " + userIdOne + " and User with id "+ userIdTwo +" not Found");
    }
}
