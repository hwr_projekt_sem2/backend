package AnonymeWuestentiere.MuellAppBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class MuellAppBackendApplication{

	public static void main(String[] args) {
		SpringApplication.run(MuellAppBackendApplication.class, args);
	}
}
