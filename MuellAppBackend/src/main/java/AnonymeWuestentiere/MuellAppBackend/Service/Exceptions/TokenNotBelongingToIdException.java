package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "AUTHENTICATION does not match requesting Id")
public class TokenNotBelongingToIdException extends RuntimeException {
    public TokenNotBelongingToIdException(){
        super("Auth does not match requesting Id");
    }
}
