package AnonymeWuestentiere.MuellAppBackend.Presentation;

import AnonymeWuestentiere.MuellAppBackend.Service.LitterService;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Litter;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/litters")
public class LitterController {
    @Autowired
    private LitterService litterService;
    @GetMapping("")
    public @ResponseBody
    ResponseEntity<Collection<Litter>> getLitters(ModelMap model) {
        return new ResponseEntity<>(litterService.getAllLitters(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public @ResponseBody
    ResponseEntity<Litter> getLitters(ModelMap model, @PathVariable("id") @NotNull Integer id) {
        return new ResponseEntity<>(litterService.getLitterById(id), HttpStatus.OK);
    }
    @GetMapping("/{latitude}/{longitude}/{radiant}")
    public @ResponseBody
    Collection<Litter> getLitters(ModelMap model, @PathVariable("latitude") @NotNull double latitude, @PathVariable("longitude") @NotNull  double longitude, @PathVariable int radiant) {
        return litterService.getLittersByCoordinatesAndRadiant(latitude,longitude,radiant);
    }
    @RequestMapping(value = "" , method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Integer> postLitter(@RequestBody Litter litter){
        litterService.postNewLitter(litter);
        return new ResponseEntity<>( null,HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}" , method = RequestMethod.PATCH, consumes = "application/json")
    public ResponseEntity<Integer> patchLitter(@RequestBody Litter litter,@PathVariable("id") @NotNull Integer id){
        litter.setId(id);
        litterService.patchLitter(litter);
        return new ResponseEntity<>(null,HttpStatus.OK);
    }
    @RequestMapping(value = "/{id}" , method = RequestMethod.DELETE)
    public ResponseEntity<Integer> deleteLitter(@PathVariable("id") @NotNull int id){
        litterService.deleteLitter(id);
        return new ResponseEntity<>(null,HttpStatus.OK);
    }
}
