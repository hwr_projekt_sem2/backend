package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Username already taken")
public class UsernameAlreadyTakenException extends RuntimeException {
    public UsernameAlreadyTakenException(){
        super("Username already taken");
    }
}



