package AnonymeWuestentiere.MuellAppBackend.Presentation;

import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.LoginFormDTO;
import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.SecurityUserDTO;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.SecurityUserDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private UserService userService;

    @Autowired
    SecurityUserDTOFactory securityUserDTOFactory;
    @RequestMapping(value = "" , method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody
    ResponseEntity<SecurityUserDTO> postUser(@RequestBody LoginFormDTO loginFormDTO){
        SecurityUserDTO securityUser= securityUserDTOFactory.createSecurityUserFromUser(userService.login(loginFormDTO.getUsername(),loginFormDTO.getPassword()));
        return new ResponseEntity<>(securityUser, HttpStatus.OK);
    }
}
