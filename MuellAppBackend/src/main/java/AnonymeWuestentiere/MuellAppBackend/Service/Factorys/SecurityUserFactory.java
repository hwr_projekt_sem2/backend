package AnonymeWuestentiere.MuellAppBackend.Service.Factorys;


import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.SecurityUser;

import java.util.ArrayList;
import java.util.Collection;


public class SecurityUserFactory {
    public SecurityUser createUserFromUserDAO(UserDAO userDAO){
        if(userDAO == null){
            return null;
        }
        return  new SecurityUser(userDAO.getId(), userDAO.getUsername(), userDAO.isVerified(), userDAO.getEmail(), userDAO.getPasswordHash(), userDAO.getToken());
    }
    public Collection<SecurityUser> createUsersFromUserDAOs(Collection<UserDAO> userDAOs){
        Collection<SecurityUser> securityUsers = new ArrayList<>();
        for(UserDAO userDAO : userDAOs){
            securityUsers.add(createUserFromUserDAO(userDAO));
        }
        return securityUsers;
    }
}
