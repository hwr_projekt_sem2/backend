package AnonymeWuestentiere.MuellAppBackend.Service.Objects;

import java.util.Collection;

public class Date {
    private int id;

    private int litter_id;

    private User owner;

    private Collection<User> participants;

    private String description;

    private java.sql.Date start;

    private java.sql.Date end;

    public Date(){}

    public Date(int id, int litter_id, User owner, String description, Collection<User> participants,java.sql.Date start, java.sql.Date end) {
        this.id = id;
        this.litter_id = litter_id;
        this.owner = owner;
        this.description = description;
        this.participants = participants;
        this.start = start;
        this.end = end;
    }

    public int getId() {
        return id;
    }

    public int getLitter_id() {
        return litter_id;
    }

    public User getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public Collection<User> getParticipants(){
        return participants;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public java.sql.Date getStart() {
        return start;
    }

    public java.sql.Date getEnd() {
        return end;
    }

    public void addParticipant(User user){
        for(User userItem:participants){
            if (userItem.getId() == user.getId()){
                return;
            }
        }
        participants.add(user);
    }
}
