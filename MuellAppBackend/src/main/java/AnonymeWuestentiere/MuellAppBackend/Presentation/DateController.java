package AnonymeWuestentiere.MuellAppBackend.Presentation;

import AnonymeWuestentiere.MuellAppBackend.Service.DateService;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Date;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dates")
public class DateController {
    @Autowired
    private DateService dateService;
    @GetMapping("/byLitter/{id}")
    public  ResponseEntity<Date> getDateByLitterId(ModelMap model,@PathVariable("id") @NotNull Integer id) {
        return new ResponseEntity<>(dateService.getDateByLitterId(id),HttpStatus.OK);
    }

    @RequestMapping(value = "" , method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Date> postDate(@RequestBody Date date,@RequestHeader(value="AUTHENTICATION") @Nullable String token){
        Date postedDate = dateService.postNewDate(date, token);
        return new ResponseEntity<>( postedDate,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}" , method = RequestMethod.PATCH, consumes = "application/json")
    public ResponseEntity<Date> userJoinDate(@PathVariable("id") @NotNull Integer id,@RequestHeader(value="AUTHENTICATION") @Nullable String token){
        Date date = dateService.joinDate(id,token);
        return new ResponseEntity<>(date,HttpStatus.ACCEPTED);
    }
}
