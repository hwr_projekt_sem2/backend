package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Token not Found")
public class TokenNotFoundException extends RuntimeException{
    public TokenNotFoundException() {
        super("TokenNotFound");
    }
}
