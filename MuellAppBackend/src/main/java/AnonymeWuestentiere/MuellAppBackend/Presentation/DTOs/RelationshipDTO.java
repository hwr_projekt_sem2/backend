package AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs;

import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;

public class RelationshipDTO {
    private int id;

    private UserDTO userActive;

    private UserDTO userPassive;

    private RelationshipType relationship_type;

    public RelationshipDTO(int id, UserDTO userActive, UserDTO userPassive, RelationshipType relationship_type) {
        this.id = id;
        this.userActive = userActive;
        this.userPassive = userPassive;
        this.relationship_type = relationship_type;
    }

    public int getId() {
        return id;
    }

    public UserDTO getUserActive() {
        return userActive;
    }

    public UserDTO getUserPassive() {
        return userPassive;
    }

    public RelationshipType getRelationship_type() {
        return relationship_type;
    }

    public void setRelationship_type(RelationshipType relationship_type) {
        this.relationship_type = relationship_type;
    }
}
