package AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.SecurityUserDTO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.SecurityUser;

public class SecurityUserDTOFactory {
        public SecurityUserDTO createSecurityUserFromUser(SecurityUser securityUser){
            if(securityUser == null){
                return null;
            }
            return  new SecurityUserDTO(securityUser.getId(), securityUser.getUsername(), securityUser.isVerified(), securityUser.getEmail(), securityUser.getToken());
        }
}
