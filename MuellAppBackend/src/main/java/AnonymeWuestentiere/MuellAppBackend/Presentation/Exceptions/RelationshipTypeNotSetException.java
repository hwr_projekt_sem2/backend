package AnonymeWuestentiere.MuellAppBackend.Presentation.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpStatusCodeException;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RelationshipTypeNotSetException extends RuntimeException {
    public RelationshipTypeNotSetException() {

        super("RelationshipType not FRIEND or BLOCKED");
    }
}
