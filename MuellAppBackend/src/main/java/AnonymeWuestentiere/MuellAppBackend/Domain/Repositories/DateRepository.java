package AnonymeWuestentiere.MuellAppBackend.Domain.Repositories;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.DateDAO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DateRepository extends CrudRepository<DateDAO, Long> {
    DateDAO findById(int id);

    DateDAO findByLitterid(@Param("Lid")int Litter_id);

}
