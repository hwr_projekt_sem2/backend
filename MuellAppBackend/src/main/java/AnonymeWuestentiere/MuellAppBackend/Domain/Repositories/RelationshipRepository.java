package AnonymeWuestentiere.MuellAppBackend.Domain.Repositories;


import AnonymeWuestentiere.MuellAppBackend.Domain.Model.RelationshipDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface RelationshipRepository extends CrudRepository<RelationshipDAO, Long> {
    @Query("select r from relationships r where r.activeUser.id = :userId OR r.passiveUser.id = :userId")
    Collection<RelationshipDAO> findAllByUserId(@Param("userId") int userId);

    @Query("select r from relationships r where (r.activeUser.id = :userId OR r.passiveUser.id = :userId) AND r.relationship_type = :relationshipType")
    Collection<RelationshipDAO> findAllByUserIdAndRelationshipId(@Param("userId") int userId,@Param("relationshipType") RelationshipType relationshipType);

    @Query("select r from relationships r where (r.activeUser.id = :userIdOne AND r.passiveUser.id  = :userIdTwo) OR (r.activeUser.id = :userIdTwo AND r.passiveUser.id = :userIdOne) ")
    RelationshipDAO findByTwoUserIds(@Param("userIdOne") int userIdOne, @Param("userIdTwo") int userIdTwo);
}
