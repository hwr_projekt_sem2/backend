package AnonymeWuestentiere.MuellAppBackend.Domain.Model;

import javax.persistence.*;

@Entity(name = "users")
public class UserDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="username")
    private String username;
    @Column(name="verified")
    private boolean verified;
    @Column(name="email")
    private String email;
    @Column(name="token")
    private String token;
    @Column(name="pw_hash")
    private String passwordHash;
    public UserDAO(){

    }
    public UserDAO(int id) {
        this.id = id;
        this.username = null;
        this.verified = false;
        this.email = null;
        this.passwordHash = null;
    }
    public UserDAO(int id, String username, boolean verified, String email, String passwordHash) {
        this.id = id;
        this.username = username;
        this.verified = verified;
        this.email = email;
        this.passwordHash = passwordHash;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getEmail() {
        return email;
    }

    public String getToken(){
        return token;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
