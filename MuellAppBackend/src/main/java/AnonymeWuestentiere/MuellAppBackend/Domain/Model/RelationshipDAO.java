package AnonymeWuestentiere.MuellAppBackend.Domain.Model;

import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;
import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipTypeConverter;

import javax.persistence.*;
import java.io.Serializable;


@Entity(name = "relationships")
public class RelationshipDAO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    @JoinColumn(name = "User_id_active")
    private UserDAO activeUser;

    @OneToOne
    @JoinColumn(name = "User_id_passive")
    private UserDAO passiveUser;

    @Convert(converter = RelationshipTypeConverter.class)
    private RelationshipType relationship_type;

    public RelationshipDAO(){}

    public RelationshipDAO(int id, UserDAO activeUser, UserDAO passiveUser, RelationshipType relationship_type) {
        this.id = id;
        this.activeUser = activeUser;
        this.passiveUser = passiveUser;
        this.relationship_type = relationship_type;
    }

    public int getId() {
        return id;
    }

    public UserDAO getActiveUser() {
        return activeUser;
    }

    public UserDAO getPassiveUser() {
        return passiveUser;
    }

    public RelationshipType getRelationship_type() {
        return relationship_type;
    }

    public void setRelationship_type(RelationshipType relationship_type) {
        this.relationship_type = relationship_type;
    }
}
