package AnonymeWuestentiere.MuellAppBackend.Service;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.LitterDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Repositories.LitterRepository;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.LitterNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.LitterFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Litter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class LitterService {
    @Autowired
    LitterRepository litterRepository;

    @Autowired
    LitterFactory litterFactory;


    public Litter getLitterById(int id){
        LitterDAO litterDAO = litterRepository.findById(id);
        if(litterDAO == null){
            throw new LitterNotFoundException(id);
        }
        return litterFactory.createLitterFromLitterDAO(litterRepository.findById(id));
    }

    public Litter getLitterByRadiant(int radiant){
        return litterFactory.createLitterFromLitterDAO(litterRepository.findByradiant(radiant));
    }

    public Collection<Litter> getAllLitters(){
        return litterFactory.createLittersFromLitterDAOs(litterRepository.findAll());
    }

    public Collection<Litter> getLittersByCoordinatesAndRadiant(double latitude, double longitude, int radiant){
        return litterFactory.createLittersFromLitterDAOs(
                litterRepository.findByLatitudeLessThanHighLatitude(latitude+radiant, latitude-radiant, longitude+radiant, longitude-radiant));
    }

    public void postNewLitter(Litter litter){
        litterRepository.save(new LitterDAO(litter.getId(), litter.getTitle(), litter.getDescription(),litter.getRadiant(),litter.isResolved(), litter.getLatitude(), litter.getLongitude()));
    }

    public void patchLitter(Litter litter){
        LitterDAO myLitter = litterRepository.findById(litter.getId());
        if(myLitter == null){
            throw new LitterNotFoundException(litter.getId());
        }
        myLitter.setTitle(litter.getTitle());
        myLitter.setDescription(litter.getDescription());
        myLitter.setRadiant(litter.getRadiant());
        myLitter.setResolved(litter.isResolved());
        litterRepository.save(myLitter);
    }

    public void deleteLitter(int id){
        litterRepository.deleteById(id);
    }

    public boolean litterExists(int id){
        LitterDAO litterDAO = litterRepository.findById(id);
        if(litterDAO == null){
            return false;
        }
        return true;
    }
}
