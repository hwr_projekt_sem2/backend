# Backend

Dies ist das Backend für das Projekt.  

## Hauptprojekt

Das Hauptprojekt inklusive dem Quellcode der App befindet sich im [Hauptprojekt](https://gitlab.com/hwr_projekt_sem2/main).  

- Repoitory: https://gitlab.com/hwr_projekt_sem2/main  
- Issues: https://gitlab.com/hwr_projekt_sem2/main/-/issues  

## Backend

Informationen zum Backend finden sich hier:

- Wiki / Dokumentation: https://gitlab.com/hwr_projekt_sem2/backend/-/wikis/home

## Installationsanleitung

Die Installationsanleitung für das Backend kann im [Wiki](https://gitlab.com/hwr_projekt_sem2/backend/-/wikis/Installationsanleitung) gefunden werden.

## Hinweise

Der *master*-Branch ist **live**!

Sämtliche Änderungen am *master*-Branch werden innerhalb weniger Minuten veröffentlicht.  
Aus Sicherheitsgründen können Änderungen nicht mehr direkt auf den *master*-Branch gespielt werden,
sondern müssen über eine [Merge-Request](https://gitlab.com/hwr_projekt_sem2/backend/-/merge_requests) eingefügt werden.
