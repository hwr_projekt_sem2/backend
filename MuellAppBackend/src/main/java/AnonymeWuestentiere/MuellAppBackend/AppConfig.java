package AnonymeWuestentiere.MuellAppBackend;

import AnonymeWuestentiere.MuellAppBackend.Presentation.Authentication.AuthenticationInterceptor;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.RelationshipDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.SecurityUserDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.UserDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.*;
import AnonymeWuestentiere.MuellAppBackend.Service.RelationshipRequestToRelationshipConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
    public class AppConfig extends WebMvcConfigurerAdapter {
    @Bean
    public SecurityUserFactory securityUserFactory() {
        return new SecurityUserFactory();
    }
    @Bean
    public UserFactory userFactory() {
        return new UserFactory();
    }
    @Bean
    public DateFactory dateFactory() {
        return new DateFactory();
    }
    @Bean
    public LitterFactory litterFactory() {
        return new LitterFactory();
    }
    @Bean
    public RelationshipFactory relationshipFactory() {
        return new RelationshipFactory() {
        };
    }
    @Bean
    public RelationshipRequestToRelationshipConverter RelationshipRequestToRelationshipConverter() {
        return new RelationshipRequestToRelationshipConverter();
    }
    @Bean
    public UserDTOFactory userDTOFactory(){
        return new UserDTOFactory();
    }
    @Bean
    public RelationshipDTOFactory relationshipDTOFactory(){
        return new RelationshipDTOFactory();
    }
    @Bean
    public SecurityUserDTOFactory securityUserDTOFactory(){
        return new SecurityUserDTOFactory();
    }
    @Bean
    AuthenticationInterceptor AuthenticationInterceptor() {
        return new AuthenticationInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(AuthenticationInterceptor()).addPathPatterns("/litters");
        registry.addInterceptor(AuthenticationInterceptor()).addPathPatterns("/users/");
        registry.addInterceptor(AuthenticationInterceptor()).addPathPatterns("/dates");
    }
}
