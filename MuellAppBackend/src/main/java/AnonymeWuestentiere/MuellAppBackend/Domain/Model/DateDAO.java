package AnonymeWuestentiere.MuellAppBackend.Domain.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "dates")
public class DateDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "litter_id")
    private int litterid;

    @Column(name = "start")
    private java.sql.Date start;

    @Column(name = "end")
    private java.sql.Date end;

    @OneToOne
    @JoinColumn(name = "Owner")
    private UserDAO owner;

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "participants",
            joinColumns = {
                    @JoinColumn(name = "Date_id", referencedColumnName = "Id",
                            nullable = false, insertable = true, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "User_id", referencedColumnName = "Id",
                            insertable = false, updatable = false)})
    Set<UserDAO> participants = new HashSet<>();



    public DateDAO(){}

    public DateDAO(int id, int litter_id, java.sql.Date start, java.sql.Date end, UserDAO owner, String description, Set<UserDAO> participants) {
        this.id = id;
        this.litterid = litter_id;
        this.start = start;
        this.end = end;
        this.owner = owner;
        this.description = description;
        this.participants = participants;
    }

    public int getId() {
        return id;
    }

    public int getLitterid() {
        return litterid;
    }

    public String getDescription() {
        return description;
    }

    public java.sql.Date getStart() {
        return start;
    }

    public java.sql.Date getEnd() {
        return end;
    }

    public UserDAO getOwner() {
        return owner;
    }

    public Set<UserDAO> getParticipants() {
        return participants;
    }

    public void addParticipant(UserDAO u){
        this.participants.add(u);
    }
    public void removeParticipant(UserDAO u){
        this.participants.removeIf(user ->(user.getId() == u.getId()));
    }
}
