package AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.RelationshipDTO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Relationship;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;

public class RelationshipDTOFactory {

    @Autowired
    UserDTOFactory userDTOFactory;

    public Collection<RelationshipDTO> createRelationshipDTOSFromRelationships(Collection<Relationship> relationships){
        if(relationships == null){
            return null;
        }
        Collection<RelationshipDTO> relationshipDTOs = new ArrayList<>();
        for (Relationship relationship: relationships){
            relationshipDTOs.add(createRelationshipDTOFromRelationship(relationship));
        }
        return relationshipDTOs;
    }

    public RelationshipDTO createRelationshipDTOFromRelationship(Relationship relationship){
        if(relationship == null){
            return null;
        }
        return new RelationshipDTO(relationship.getId(), userDTOFactory.createUserDTOFromUser(relationship.getUserActive()), userDTOFactory.createUserDTOFromUser(relationship.getUserPassive()), relationship.getRelationship_type());
    }
}
