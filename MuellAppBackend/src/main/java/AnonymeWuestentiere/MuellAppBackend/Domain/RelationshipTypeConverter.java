package AnonymeWuestentiere.MuellAppBackend.Domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RelationshipTypeConverter implements AttributeConverter<RelationshipType, Integer> {

    public Integer convertToDatabaseColumn(RelationshipType value) {
        if ( value == null ) {
            return null;
        }

        return value.getCode();
    }

    public RelationshipType convertToEntityAttribute(Integer value) {
        if ( value == null ) {
            return null;
        }

        return RelationshipType.fromCode(value);
    }
}
