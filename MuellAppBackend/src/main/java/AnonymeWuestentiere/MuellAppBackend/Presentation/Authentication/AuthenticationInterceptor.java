package AnonymeWuestentiere.MuellAppBackend.Presentation.Authentication;


import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.LoginFailedException;
import AnonymeWuestentiere.MuellAppBackend.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserService userService;

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) {
        }

        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        }

        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

            HandlerMethod handlerMethod = (HandlerMethod) handler;

            String token = request.getHeader("AUTHENTICATION");

            if(token == null) {
                throw new LoginFailedException();
            }
            if(!userService.getTokenExists(token)){
                throw new LoginFailedException();
            }
            return true;
        }
}
