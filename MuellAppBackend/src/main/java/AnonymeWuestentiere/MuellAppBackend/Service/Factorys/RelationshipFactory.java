package AnonymeWuestentiere.MuellAppBackend.Service.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.RelationshipDAO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Relationship;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;

public class RelationshipFactory {

    @Autowired
    UserFactory userFactory;

    public Collection<Relationship> createRelationshipsFromRelationshipDAOs(Collection<RelationshipDAO> relationshipDAOs){
        if(relationshipDAOs == null){
           return null;
        }
        Collection<Relationship> relationships = new ArrayList<>();
        for (RelationshipDAO relationshipDAO: relationshipDAOs){
            relationships.add(createRelationshipFromRelationshipDAO(relationshipDAO));
        }
        return relationships;
    }

    public Relationship createRelationshipFromRelationshipDAO(RelationshipDAO relationshipDAO){
        if(relationshipDAO == null){
            return null;
        }
        return new Relationship(relationshipDAO.getId(),
                    userFactory.createUserFromIdAndUsername(relationshipDAO.getActiveUser().getId(), relationshipDAO.getActiveUser().getUsername()),
                    userFactory.createUserFromIdAndUsername(relationshipDAO.getPassiveUser().getId(), relationshipDAO.getPassiveUser().getUsername()),
                relationshipDAO.getRelationship_type());
    }
}
