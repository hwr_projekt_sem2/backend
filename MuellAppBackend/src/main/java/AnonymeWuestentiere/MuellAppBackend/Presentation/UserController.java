package AnonymeWuestentiere.MuellAppBackend.Presentation;

import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;
import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.RelationshipDTO;
import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.SecurityUserDTO;
import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.UserDTO;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Exceptions.RelationshipTypeNotSetException;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.RelationshipDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.SecurityUserDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys.UserDTOFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.TokenNotBelongingToIdException;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.SecurityUser;
import AnonymeWuestentiere.MuellAppBackend.Service.RelationshipService;
import AnonymeWuestentiere.MuellAppBackend.Service.UserService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDTOFactory userDTOFactory;

    @Autowired
    private RelationshipService relationshipService;

    @Autowired
    private SecurityUserDTOFactory securityUserDTOFactory;

    @Autowired
    private RelationshipDTOFactory relationshipDTOFactory;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<UserDTO> getUserbyId(ModelMap model, @PathVariable @NotNull Integer id) {
        return new ResponseEntity<>(userDTOFactory.createUserDTOFromUser(userService.getUserById(id)), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}/relationships", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Collection<RelationshipDTO>> getRelationshipsByUserId(ModelMap model, @RequestHeader(value="AUTHENTICATION") @Nullable String token, @PathVariable @NotNull Integer id, @RequestParam("filter") @Nullable String filter) {
        if(!userService.isSendingUserTokenBelongingToSendingUserId(id, token)){
            throw new TokenNotBelongingToIdException();
        }
        if(filter != null) {
            RelationshipType relationshipType = RelationshipType.valueOf(filter.toUpperCase());
            return new ResponseEntity<>(relationshipDTOFactory.createRelationshipDTOSFromRelationships(relationshipService.getRelationshipsByUserIdAndRelationshipType(id, relationshipType)),HttpStatus.OK);
        }
        return new ResponseEntity<>(relationshipDTOFactory.createRelationshipDTOSFromRelationships(relationshipService.getRelationshipsByUserId(id)), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}/relationships/{id2}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Integer> postRelationshipsByUserId(ModelMap model, @RequestHeader(value="AUTHENTICATION")  @Nullable String token, @PathVariable @NotNull Integer id, @PathVariable @NotNull Integer id2,@RequestParam("type") @Nullable String type) {
        if(type == null){
            throw new RelationshipTypeNotSetException();
        }
        if(!type.toUpperCase().equals("FRIEND") && !type.toUpperCase().equals("BLOCKED") && !type.toUpperCase().equals("UNFRIEND")){
            throw new RelationshipTypeNotSetException();
        }
        relationshipService.postNewRelationship(id,id2,type.toUpperCase(), token);
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }



    @RequestMapping(value = "/{idOne}/relationships/{idTwo}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<RelationshipDTO> getRelationshipsByTwoUserIds(ModelMap model,@RequestHeader(value="AUTHENTICATION") @Nullable String token, @PathVariable @NotNull Integer idOne, @PathVariable @NotNull Integer idTwo) {
        if(!userService.isSendingUserTokenBelongingToSendingUserId(idOne, token)){
            throw new TokenNotBelongingToIdException();
        }

        return new ResponseEntity<>(relationshipDTOFactory.createRelationshipDTOFromRelationship(relationshipService.getRelationshipByTwoUserIds(idOne,idTwo)),HttpStatus.OK);
    }

    @RequestMapping(value = "" , method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<SecurityUserDTO> postUser(@RequestBody SecurityUser securityUser){
            SecurityUserDTO securityUserDTO = securityUserDTOFactory.createSecurityUserFromUser(userService.postNewUser(securityUser));
            return new ResponseEntity<>(securityUserDTO, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/search/{username}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Collection<UserDTO>> getUsersbyusername(ModelMap model, @PathVariable String username) {
        return new ResponseEntity<>(userDTOFactory.createUserDTOsFromUsers(userService.searchUsersByUsername(username)), HttpStatus.OK);
    }
}
