package AnonymeWuestentiere.MuellAppBackend.Service;

import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Relationship;
import org.springframework.beans.factory.annotation.Autowired;

public class RelationshipRequestToRelationshipConverter {
    @Autowired
    UserService userService;

    public Relationship convertRelationshipRequestToRelationship(int sendingUserId, int receivingUserId, String requestedRelationshipStatus, Relationship currentRelationship){
        if(sendingUserId < receivingUserId) {
            return convertOutgoingFriendRequest(sendingUserId,receivingUserId,requestedRelationshipStatus, currentRelationship);
        }

        if(sendingUserId > receivingUserId){
            return convertIncomingFriendRequest(sendingUserId,receivingUserId,requestedRelationshipStatus, currentRelationship);
        }
        return null;
    }

    private Relationship convertOutgoingFriendRequest(int sendingUserId, int receivingUserId, String requestedRelationshipStatus, Relationship currentRelationship){
        if (currentRelationship == null) {
            Relationship newRelationship;
            if (RelationshipType.valueOf(requestedRelationshipStatus) == RelationshipType.FRIEND) {
                newRelationship = new Relationship(0, userService.getUserById(sendingUserId), userService.getUserById(receivingUserId), RelationshipType.OUTGOINGFRIEND);
                return newRelationship;
            }
            return null;
        }
        if (currentRelationship.getRelationship_type() == RelationshipType.INCOMINGFRIEND) {
            currentRelationship = new Relationship(currentRelationship.getId(), userService.getUserById(sendingUserId), userService.getUserById(receivingUserId), RelationshipType.FRIEND);
            return currentRelationship;
        }
        if (currentRelationship.getRelationship_type() == RelationshipType.FRIEND) {
            return currentRelationship;
        }
        if (currentRelationship.getRelationship_type() == RelationshipType.OUTGOINGFRIEND) {
            return currentRelationship;
        }
        return null;
    }

    private Relationship convertIncomingFriendRequest(int sendingUserId, int receivingUserId, String requestedRelationshipStatus, Relationship currentRelationship){
        if(currentRelationship == null) {
            Relationship newRelationship;
            if (RelationshipType.valueOf(requestedRelationshipStatus) == RelationshipType.FRIEND){
                newRelationship = new Relationship(0, userService.getUserById(receivingUserId), userService.getUserById(sendingUserId), RelationshipType.INCOMINGFRIEND);
                return newRelationship;
            }
            return null;
        }
        if(currentRelationship.getRelationship_type() == RelationshipType.OUTGOINGFRIEND){
            currentRelationship = new Relationship(currentRelationship.getId(), userService.getUserById(receivingUserId), userService.getUserById(sendingUserId), RelationshipType.FRIEND);
            return currentRelationship;
        }

        if(currentRelationship.getRelationship_type() == RelationshipType.FRIEND){
            return currentRelationship;
        }
        if(currentRelationship.getRelationship_type() == RelationshipType.INCOMINGFRIEND){
            return currentRelationship;
        }
        return null;
    }
}
