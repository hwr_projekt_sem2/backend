package AnonymeWuestentiere.MuellAppBackend.Service;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Repositories.UserRepository;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.*;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.SecurityUserFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.UserFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.SecurityUser;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.User;
import AnonymeWuestentiere.MuellAppBackend.Service.Passwords.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;

//Schnittstelle zwischen Presentation und Domain
// Hier werden alle Verarbeitungen durchgeführt
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    UserFactory userFactory;
    @Autowired
    SecurityUserFactory securityUserFactory;

    PasswordEncoder passwordEncoder = new PasswordEncoder();



    public User getUserById(int id){
        UserDAO userDAO = userRepository.findById(id);
        if(userDAO == null){
            throw new UserNotFoundException(id);
        }
        return userFactory.createUserFromIdAndUsername(userDAO.getId(),userDAO.getUsername());
    }

    public Collection<User> searchUsersByUsername(String username){
        Collection<UserDAO> userDAOs = userRepository.findUsersByUsername(username);
        return userFactory.createUsersFromUserDAOs(userDAOs);
    }

    public SecurityUser postNewUser(SecurityUser securityUser){
        UserDAO userDAO= new UserDAO(securityUser.getId(), securityUser.getUsername(), securityUser.isVerified(), securityUser.getEmail(), passwordEncoder.encodePassword(securityUser.getPassword()));
        String token = UUID.randomUUID().toString();
        userDAO.setToken(token);
        if(usernameExists(securityUser.getUsername())){
            throw  new UsernameAlreadyTakenException();
        }
        if(emailExists(securityUser.getEmail())){
            throw new EmailAlreadyTakenException();
        }
        userRepository.save(userDAO);
        return securityUserFactory.createUserFromUserDAO(userDAO);
    }
    private boolean emailExists(String email){
        if(userRepository.getUserByEmail(email)!= null){
            return true;
        }
        return false;
    }
    private boolean usernameExists(String username){
        if(userRepository.getUserByUsername(username.toLowerCase())!= null){
            return true;
        }
        return false;
    }

    public SecurityUser login(String username, String password) {
        UserDAO userDAO = userRepository.getUserByUsername(username);
        if (userDAO != null) {
            if(passwordEncoder.verifyPassword(password,userDAO.getPasswordHash())) {
                String token = UUID.randomUUID().toString();
                userDAO.setToken(token);
                userRepository.save(userDAO);
                return securityUserFactory.createUserFromUserDAO(userDAO);
            }
        }else{
            userDAO = userRepository.getUserByEmail(username);
            if (userDAO != null){
                if(passwordEncoder.verifyPassword(password,userDAO.getPasswordHash())) {
                    String token = UUID.randomUUID().toString();
                    userDAO.setToken(token);
                    userRepository.save(userDAO);
                    return securityUserFactory.createUserFromUserDAO(userDAO);
                }
            }
        }
        throw new LoginFailedException();
    }

    public boolean getUserExists(int id){
        UserDAO userDAO = userRepository.findById(id);
        if (userDAO == null){
            return false;
        }
        return true;
    }

    public boolean getTokenExists(String token){
        UserDAO userDAO = userRepository.findUserByToken(token);
        if (userDAO == null){
            return false;
        }
        return true;
    }

    public User getUserByToken(String token){
        if(!getTokenExists(token)){
            throw new TokenNotFoundException();
        }
        UserDAO userDAO = userRepository.findUserByToken(token);
        return userFactory.createUserFromIdAndUsername(userDAO.getId(),userDAO.getUsername());
    }

    public boolean isSendingUserTokenBelongingToSendingUserId(int id, String token){
        UserDAO userDAO = userRepository.findById(id);
        if(userDAO == null){
            return false;
        }
        if(userDAO.getToken().equals(token)){
            return true;
        }
        return false;
    }
}
