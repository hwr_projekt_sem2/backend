package AnonymeWuestentiere.MuellAppBackend.Presentation.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs.UserDTO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.SecurityUser;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.User;

import java.util.ArrayList;
import java.util.Collection;

public class UserDTOFactory {
    public UserDTO createUserDTOFromUser(User user){
        if(user == null){
            return null;
        }
        return  new UserDTO(user.getId(), user.getUsername());
    }
    public Collection<UserDTO> createUserDTOsFromUsers(Collection<User> users){
        Collection<UserDTO> userDTOs = new ArrayList<>();
        for(User user : users){
            userDTOs.add(createUserDTOFromUser(user));
        }
        return userDTOs;
    }
}
