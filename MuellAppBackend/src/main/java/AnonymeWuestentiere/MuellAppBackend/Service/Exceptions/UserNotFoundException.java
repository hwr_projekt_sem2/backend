package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User with given Id was not found")
public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException() {
        super("Token not Found");
    }
    public UserNotFoundException(int id) {
        super("User with id: " + id + " not found");
    }
}
