package AnonymeWuestentiere.MuellAppBackend.Service.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.LitterDAO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Litter;

import java.util.ArrayList;
import java.util.Collection;

public class LitterFactory {
    public Litter createLitterFromLitterDAO(LitterDAO litterDAO){
        return new Litter(litterDAO.getId(),litterDAO.getTitle(), litterDAO.getDescription(),litterDAO.getRadiant(),litterDAO.isResolved(), litterDAO.getLatitude(), litterDAO.getLongitude());
    }
    public Collection<Litter> createLittersFromLitterDAOs(Collection<LitterDAO> litterDAOs){
        Collection<Litter> litters = new ArrayList<>();
        for(LitterDAO litterDAO : litterDAOs){
            litters.add(createLitterFromLitterDAO(litterDAO));
        }
        return litters;
    }
}
