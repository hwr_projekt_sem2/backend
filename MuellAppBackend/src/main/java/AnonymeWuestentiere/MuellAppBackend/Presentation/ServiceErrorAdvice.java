package AnonymeWuestentiere.MuellAppBackend.Presentation;

import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ServiceErrorAdvice {
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<String> handleRunTimeException(RuntimeException e) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler({UsernameAlreadyTakenException.class, EmailAlreadyTakenException.class, LitterAlreadyHasDateException.class})
    public ResponseEntity<String> handleBadRequestException(RuntimeException e) {
        return error(HttpStatus.BAD_REQUEST, e);
    }
    @ExceptionHandler({LitterNotFoundException.class, RelationshipNotFoundException.class, DateNotFoundException.class})
    public ResponseEntity<String> handleNotFoundException(RuntimeException e) {
        return error(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler({LoginFailedException.class})
    public void handleForbiddenException(HttpServletResponse response)throws IOException {
        response.sendError(HttpStatus.FORBIDDEN.value());
    }

    private ResponseEntity<String> error(HttpStatus status, Exception e) {
        return ResponseEntity.status(status).body(e.getMessage());
    }
}
