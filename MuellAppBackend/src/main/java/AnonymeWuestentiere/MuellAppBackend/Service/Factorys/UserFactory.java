package AnonymeWuestentiere.MuellAppBackend.Service.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.User;

import java.util.ArrayList;
import java.util.Collection;

public class UserFactory {
    public User createUserFromIdAndUsername(int id, String username){
        if(id == 0){
            return null;
        }
        return  new User(id, username);
    }

    public Collection<User> createUsersFromUserDAOs(Collection<UserDAO> userDAOs){
        Collection<User> users = new ArrayList<>();
        for(UserDAO userDAO: userDAOs){
            users.add(createUserFromIdAndUsername(userDAO.getId(),userDAO.getUsername()));
        }
        return users;
    }
}
