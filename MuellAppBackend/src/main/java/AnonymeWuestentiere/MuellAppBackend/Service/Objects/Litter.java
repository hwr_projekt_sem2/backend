package AnonymeWuestentiere.MuellAppBackend.Service.Objects;

public class Litter {
    private int id;

    private String description;
    private String title;
    private int radiant;
    private boolean resolved;
    private double latitude;
    private double longitude;

    public Litter(){}

    public Litter(int id,String title, String description, int radiant, boolean resolved, double latitude, double longitude) {
        this.id = id;
        this.description = description;
        this.title = title;
        this.radiant = radiant;
        this.resolved = resolved;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle(){
        return title;
    }

    public int getRadiant() {
        return radiant;
    }

    public boolean isResolved() {
        return resolved;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title){
        this.title = title;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public void setRadiant(int radiant) {
        this.radiant = radiant;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }
}
