package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Email already taken")
    public class EmailAlreadyTakenException extends RuntimeException {
        public EmailAlreadyTakenException(){
            super("Email already taken");
        }
}
