package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "No user with entered credentials found")
public class LoginFailedException extends RuntimeException{
    public LoginFailedException(){super( "No user with entered credentials found");}
}
