package AnonymeWuestentiere.MuellAppBackend.Service.Passwords;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class PasswordEncoder {
    final int strength = 12;

    public String encodePassword(String plain_password) {
        return BCrypt.withDefaults().hashToString(strength, plain_password.toCharArray());
    }

    public Boolean verifyPassword(String plain_password, String hashed_password) {
        BCrypt.Result result = BCrypt.verifyer().verify(plain_password.toCharArray(), hashed_password);
        return result.verified;
    }
}
