package AnonymeWuestentiere.MuellAppBackend.Service.Objects;

import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;


public class Relationship {

    private int id;

    private User securityUserActive;

    private User securityUserPassive;

    private RelationshipType relationship_type;

    public Relationship(int id, User securityUserActive, User securityUserPassive, RelationshipType relationship_type) {
        this.id = id;
        this.securityUserActive = securityUserActive;
        this.securityUserPassive = securityUserPassive;
        this.relationship_type = relationship_type;
    }

    public int getId() {
        return id;
    }

    public User getUserActive() {
        return securityUserActive;
    }

    public User getUserPassive() {
        return securityUserPassive;
    }

    public RelationshipType getRelationship_type() {
        return relationship_type;
    }

    public void setRelationship_type(RelationshipType relationship_type) {
        this.relationship_type = relationship_type;
    }
}
