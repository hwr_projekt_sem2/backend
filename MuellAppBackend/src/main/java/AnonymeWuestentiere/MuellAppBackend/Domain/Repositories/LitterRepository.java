package AnonymeWuestentiere.MuellAppBackend.Domain.Repositories;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.LitterDAO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface LitterRepository extends CrudRepository<LitterDAO, Long> {
    LitterDAO findById(int id);
    LitterDAO findByradiant(int radiant);
    @Query("select l from litters l where l.latitude between :lowLatitude AND :highLatitude AND l.longitude between :lowLongitude AND :highLongitude")
    Collection<LitterDAO> findByLatitudeLessThanHighLatitude(@Param("highLatitude")double highLatitude, @Param("lowLatitude")double lowLatitude, @Param("highLongitude")double highLongitude,@Param("lowLongitude") double lowLongitude);

    Collection<LitterDAO> findAll();
    @Transactional
    void deleteById(int id);
}
