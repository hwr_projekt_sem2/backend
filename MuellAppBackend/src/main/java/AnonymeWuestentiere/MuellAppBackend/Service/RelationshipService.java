package AnonymeWuestentiere.MuellAppBackend.Service;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.RelationshipDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.RelationshipType;
import AnonymeWuestentiere.MuellAppBackend.Domain.Repositories.RelationshipRepository;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.RelationshipNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.TokenNotBelongingToIdException;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.UserNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.RelationshipFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RelationshipService {

    @Autowired
    private RelationshipFactory relationshipFactory;

    @Autowired
    private RelationshipRepository relationshipRepository;

    @Autowired
    private RelationshipRequestToRelationshipConverter relationshipRequestToRelationshipConverter;

    @Autowired
    private UserService userService;

    public Collection<Relationship> getRelationshipsByUserId(int userId) {
        return relationshipFactory.createRelationshipsFromRelationshipDAOs(relationshipRepository.findAllByUserId(userId));
    }

    public Collection<Relationship> getRelationshipsByUserIdAndRelationshipType(int userId, RelationshipType relationshipType) {
        return relationshipFactory.createRelationshipsFromRelationshipDAOs(relationshipRepository.findAllByUserIdAndRelationshipId(userId, relationshipType));
    }

    public Relationship getRelationshipByTwoUserIds(int userIdOne, int userIdTwo) {
        RelationshipDAO relationshipDAO = relationshipRepository.findByTwoUserIds(userIdOne, userIdTwo);
        if (relationshipDAO == null) {
            throw new RelationshipNotFoundException(userIdOne, userIdTwo);
        }
        return relationshipFactory.createRelationshipFromRelationshipDAO(relationshipDAO);
    }

    public void postNewRelationship(int sendingUserId, int receivingUserId, String relationshipType, String token) {

        Relationship relationship = null;
        if (!userService.getUserExists(sendingUserId)) {
            throw new UserNotFoundException(sendingUserId);
        }
        if (!userService.isSendingUserTokenBelongingToSendingUserId(sendingUserId,token)){
            throw new TokenNotBelongingToIdException();
        }
        if (!userService.getUserExists(receivingUserId)) {
            throw new UserNotFoundException(receivingUserId);
        }
        try {
            relationship = getRelationshipByTwoUserIds(sendingUserId, receivingUserId);
        } catch (RelationshipNotFoundException r) {
        }


        if (relationshipType.equals("FRIEND")) {
            relationship = relationshipRequestToRelationshipConverter.convertRelationshipRequestToRelationship(sendingUserId, receivingUserId, relationshipType, relationship);
            relationshipRepository.save(
                    new RelationshipDAO(relationship.getId(),
                            new UserDAO(relationship.getUserActive().getId(), null, false, null, null),
                            new UserDAO(relationship.getUserPassive().getId(), null, false, null, null),
                            relationship.getRelationship_type()));
        }
        if(relationshipType.equals("UNFRIEND") && relationship != null){
            if(relationship.getRelationship_type() == RelationshipType.FRIEND || relationship.getRelationship_type() == RelationshipType.OUTGOINGFRIEND || relationship.getRelationship_type() == RelationshipType.INCOMINGFRIEND)
            relationshipRepository.delete(new RelationshipDAO(relationship.getId(),
                    new UserDAO(relationship.getUserActive().getId(), null, false, null, null),
                    new UserDAO(relationship.getUserPassive().getId(), null, false, null, null),
                    relationship.getRelationship_type()));
        }

    }


}
