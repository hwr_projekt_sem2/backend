package AnonymeWuestentiere.MuellAppBackend.Domain.Model;

import javax.persistence.*;

@Entity(name = "litters")
public class LitterDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @Column(name = "radiant")
    private int radiant;

    @Column(name = "resolved")
    private boolean resolved;

    @Column( name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    public LitterDAO(){}

    public LitterDAO(int id,String title, String description,  int radiant, boolean resolved, double latitude, double longitude) {
        this.id = id;
        this.description = description;
        this.title = title;
        this.radiant = radiant;
        this.resolved = resolved;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle(){
        return title;
    }
    public int getRadiant() {
        return radiant;
    }

    public boolean isResolved() {
        return resolved;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setRadiant(int radiant) {
        this.radiant = radiant;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }
}
