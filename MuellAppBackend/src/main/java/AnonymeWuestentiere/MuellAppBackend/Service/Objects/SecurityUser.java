package AnonymeWuestentiere.MuellAppBackend.Service.Objects;


public class SecurityUser {

        private int id;
        private String username;
        private boolean verified;
        private String email;
        private String password;
        private String token;


        public SecurityUser(int id, String username, boolean verified, String email, String password, String token) {
            this.id = id;
            this.username = username;
            this.verified = verified;
            this.email = email;
            this.password = password;
            this.token = token;
        }

        public int getId() {
            return id;
        }

        public String getUsername() {
            return username;
        }

        public boolean isVerified() {
            return verified;
        }

        public String getEmail() {
            return email;
        }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public void setUsername(String username) {
            this.username = username;
        }

        public void setVerified(boolean verified) {
            this.verified = verified;
        }

    public void setPassword(String password) {
        this.password = password;
    }
}
