package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Date with given id not found")
public class DateNotFoundException extends RuntimeException{
    public DateNotFoundException(int id) {
        super("Date with id: " + id + " not found");
    }
}