package AnonymeWuestentiere.MuellAppBackend.Presentation.DTOs;


public class SecurityUserDTO {
    private int id;
    private String username;
    private boolean verified;
    private String email;
    private String token;

    public SecurityUserDTO(int id, String username, boolean verified, String email, String token) {
        this.id = id;
        this.username = username;
        this.verified = verified;
        this.email = email;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }
}
