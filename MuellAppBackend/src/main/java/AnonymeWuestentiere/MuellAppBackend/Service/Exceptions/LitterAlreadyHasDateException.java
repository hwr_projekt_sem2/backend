package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason = "Litter with given id not found")
public class LitterAlreadyHasDateException extends RuntimeException {
    public LitterAlreadyHasDateException(int id) {
        super("Litter with id: " + id + " already has a date");
    }
}

