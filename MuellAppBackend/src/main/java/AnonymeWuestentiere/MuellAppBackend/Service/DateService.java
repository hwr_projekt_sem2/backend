package AnonymeWuestentiere.MuellAppBackend.Service;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.DateDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Repositories.DateRepository;
import AnonymeWuestentiere.MuellAppBackend.Domain.Repositories.UserRepository;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.DateNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.LitterAlreadyHasDateException;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.LitterNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Exceptions.UserNotFoundException;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.DateFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Factorys.UserFactory;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Date;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashSet;
import java.util.Set;

@Service
public class DateService {

    @Autowired
    DateRepository dateRepository;

    @Autowired
    DateFactory dateFactory;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserFactory UserFactory;

    @Autowired
    LitterService litterService;

    public Date getDateByLitterId(int litterId){
        DateDAO dateDAO = dateRepository.findByLitterid(litterId);
        if(dateDAO == null){
            return null;
        }
        return dateFactory.createDateFromDateDAO(dateDAO);
    }

    public Date getDateById(int id){
        DateDAO dateDAO = dateRepository.findById(id);
        if(dateDAO == null){
            return null;
        }
        return dateFactory.createDateFromDateDAO(dateRepository.findById(id));
    }

    public Date postNewDate(Date date, String token){
        if(!litterService.litterExists(date.getLitter_id())){
            throw new LitterNotFoundException(date.getLitter_id());
        }
        if(getDateByLitterId(date.getLitter_id())!= null){
            throw new LitterAlreadyHasDateException(date.getLitter_id());
        }
        UserDAO user = userRepository.findUserByToken(token);
        if(user == null){
            throw new UserNotFoundException();
        }
        DateDAO dateDAO = dateRepository.save(new DateDAO(
                        0,
                        date.getLitter_id(),
                        date.getStart(),
                        date.getEnd(),
                new UserDAO(user.getId(),user.getUsername(),user.isVerified(),user.getEmail(),user.getPasswordHash()),
                date.getDescription(),
                new HashSet<UserDAO>()));

        return dateFactory.createDateFromDateDAO(dateDAO);
    }

    public Date joinDate(int dateId, String token){
        if(getDateById(dateId)== null){
            throw new DateNotFoundException(dateId);
        }
        UserDAO user = userRepository.findUserByToken(token);
        if(user == null){
            throw new UserNotFoundException();
        }
        DateDAO dateDAO = dateRepository.findById(dateId);
        dateDAO.addParticipant(user);
        DateDAO newDateDAO = dateRepository.save(dateDAO);
        return dateFactory.createDateFromDateDAO(newDateDAO);
    }
}
