package AnonymeWuestentiere.MuellAppBackend.Domain.Repositories;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;


public interface UserRepository extends CrudRepository<UserDAO, Long> {

    UserDAO findById(int id);

    @Query("SELECT u FROM users u where u.username = :username and u.passwordHash = :passwordHash")
    UserDAO findUserByUsernameAndPassword(@Param("username") String username, @Param("passwordHash") String passwordHash);

    @Query("SELECT u FROM users u where u.email = :email and u.passwordHash = :passwordHash")
    UserDAO findUserByEmailAndPassword(@Param("email") String email, @Param("passwordHash") String passwordHash);

    @Query("SELECT u FROM users u where u.username LIKE %:username%")
    Collection<UserDAO> findUsersByUsername(@Param("username") String username);

    @Query("SELECT u FROM users u where u.email = :email")
    UserDAO getUserByEmail(@Param("email") String email);

    @Query("SELECT u FROM users u where LOWER(u.username) = LOWER(:username)")
    UserDAO getUserByUsername(@Param("username") String username);

    @Query("SELECT u FROM users u where u.token = :token")
    UserDAO findUserByToken(@Param("token") String token);

}
