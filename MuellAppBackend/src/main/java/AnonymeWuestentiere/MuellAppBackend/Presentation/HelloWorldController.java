package AnonymeWuestentiere.MuellAppBackend.Presentation;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloworld")
public class HelloWorldController {
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String printHello(ModelMap model) {
        return "HelloWorld";
    }
}
