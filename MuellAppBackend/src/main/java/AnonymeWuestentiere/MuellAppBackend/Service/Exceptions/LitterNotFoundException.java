package AnonymeWuestentiere.MuellAppBackend.Service.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "Litter with given id not found")
public class LitterNotFoundException extends RuntimeException {

    public LitterNotFoundException(int id) {
        super("Litter with id: " + id + " not found");
    }
}
