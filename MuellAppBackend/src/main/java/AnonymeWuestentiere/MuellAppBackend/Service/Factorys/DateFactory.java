package AnonymeWuestentiere.MuellAppBackend.Service.Factorys;

import AnonymeWuestentiere.MuellAppBackend.Domain.Model.DateDAO;
import AnonymeWuestentiere.MuellAppBackend.Domain.Model.UserDAO;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.Date;
import AnonymeWuestentiere.MuellAppBackend.Service.Objects.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class DateFactory {
@Autowired
UserFactory userFactory;
    public Date createDateFromDateDAO(DateDAO dateDAO){
        Set<User> participants = new HashSet<>(userFactory.createUsersFromUserDAOs(dateDAO.getParticipants()));

        return new Date(dateDAO.getId(), dateDAO.getLitterid(),
                userFactory.createUserFromIdAndUsername(
                        dateDAO.getOwner().getId(),
                        dateDAO.getOwner().getUsername()),
                        dateDAO.getDescription(),
                participants,
                dateDAO.getStart(),
                dateDAO.getEnd()
                );
    }
}
